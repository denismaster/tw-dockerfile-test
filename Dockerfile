FROM mcr.microsoft.com/dotnet/sdk:8.0 AS builder 
WORKDIR /app

COPY . ./
RUN dotnet restore
RUN dotnet publish ./src/TestProject.Api -c Release

FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app
COPY --from=builder /app/src/TestProject.Api/bin/Release/net8.0/publish/ .
ENTRYPOINT ["dotnet", "TestProject.Api.dll"]
